package com.softzino.diapp.dagger.components;

import com.softzino.diapp.dagger.annotations.AppScope;
import com.softzino.diapp.dagger.modules.AppModule;
import com.softzino.diapp.dagger.modules.NetModule;
import com.softzino.diapp.dagger.modules.VehicleModule;
import com.softzino.diapp.ui.home.MainActivity;

import dagger.Component;
import retrofit2.Retrofit;

@AppScope
@Component(modules = {AppModule.class,NetModule.class})
public interface AppComponent {
    Retrofit retrofit();
}
